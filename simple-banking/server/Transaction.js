const User = require("./models/user-model");
const Trans = require("./models/transaction-model")
const jwt = require("jsonwebtoken");
const { db } = require("./models/transaction-model");

const Transaction = async (req, res) => {

    const transactions = await Trans.find({
        transactionType: req.body.transactionType,
        transactionAmount: req.body.transactionAmount,
        sendAccount: req.body.sendAccount,
    });

    if (transactions) {
        res.json({ status: "ok", trans: transactions });
    } else {
        res.json({status: "error", error })
    }

}