const User = require("./models/user-model");
const jwt = require("jsonwebtoken");

const AddBalance = async (req, res) => {
  const token = await jwt.decode(req.headers.token);
  const user = await User.findOne({ email: token.email });
  const amount = req.body.balance;
  const balance = user.balance;
  const newBalance = balance + amount;

  if (amount > 0) {
    try {
      await User.updateOne(
        { email: token.email },
        { $set: { balance: newBalance } }
      );

      return res.json({ status: "ok" });
    } catch (error) {
      return res.json({ status: "error", message: error });
    }
  } else {
    return res.json({ status: "error", message: "amount is less than 0" });
  }
};

module.exports = AddBalance;
