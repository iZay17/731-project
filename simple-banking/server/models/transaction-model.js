const mongoose = require("mongoose");

const TransactionSchema = mongoose.Schema(
  {
    transactionType: { type: String, required: true },
    transactionAmount: { type: Number, required: true },
    sendAccount: { type: String, required: true },
    receiveAccount: { type: Number, required: true },
  },
  {
    collection: "transaction-data",
  }
);

const Transaction = mongoose.model("TransactionData", TransactionSchema);
module.exports = Transaction;