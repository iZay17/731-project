const mongoose = require("mongoose");

const UserSchema = mongoose.Schema(
  {
    status: { type: String, required: true},
    fName: { type: String, required: true },
    lName: { type: String, required: true },
    password: { type: String, required: true },
    email: { type: String, unique: true, required: true },
    phoneNumber: { type: Number, unique: true, required: true },
    balance: { type: Number, default: 0, required: false },
    saving: { type: Number, default: 0 },
    creditCard: { type: Number, default: 0 },
  },
  {
    collection: "user-data",
  }
);

const User = mongoose.model("UserData", UserSchema);
module.exports = User;
