const User = require("./models/user-model");

const Register = async (req, res) => {
  try {
    console.log(req.body);
    await User.create({
      fName: req.body.fName,
      lName: req.body.lName,
      email: req.body.email,
      phoneNumber: req.body.phoneNumber,
      password: req.body.password,
      status: req.body.status,
    });
    res.json({ status: "ok" });
  } catch (error) {
    res.json({ status: "error", Message: error });
  }
};

module.exports = Register;
