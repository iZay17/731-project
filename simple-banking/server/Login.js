const User = require("./models/user-model");
const jwt = require("jsonwebtoken");

const Login = async (req, res) => {
  const user = await User.findOne({
    email: req.body.email,
    password: req.body.password,
  });
  if (user) {
    const token = jwt.sign(
      {
        email: user.email,
        password: user.password,
      },
      "secret123"
    );
    res.json({ status: "ok", userData: token });
  } else {
    res.json({ status: "Error", userData: false });
  }
};
module.exports = Login;
