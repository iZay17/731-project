const User = require("./models/user-model");
const jwt = require("jsonwebtoken");

const SearchAccounts = async (req, res) => {
  const user = await User.findOne({
    email: req.body.email,
  });
  if (user) {
    const token = jwt.sign(
      {
        email: user.email,
      },
      "secret123"
    );
    res.json({ status: "ok", userData: token });
  } else {
    res.json({ status: "Error", userData: false });
  }
};
module.exports = SearchAccounts;