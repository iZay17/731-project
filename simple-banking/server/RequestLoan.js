const User = require("./models/user-model");
const jwt = require("jsonwebtoken");
const RequestLoan = async (req, res) => {
  const amount = req.body.amount;
  const token = jwt.decode(req.headers.token);

  const loan = async (amount, userEmail) => {
    try {
      await User.updateOne(
        { email: userEmail },
        { $set: { creditCard: amount } }
      );
    } catch (error) {
      res.json({ status: "Error", message: error });
    }
  };

  if (amount > 0) {
    try {
      const user = await User.findOne({ email: token.email });

      const userCredit = (user.creditCard += amount);

      if (req.body.loanType === "fixed") {
        loan(userCredit, token.email);
        res.json({ status: "ok" });
      } else if (req.body.loanType === "variable") {
        loan(userCredit, email);
        res.json({ status: "ok" });
      }
    } catch (error) {
      res.json({ status: "Error", message: error });
    }
  } else {
    res.json({ status: "Error", message: "Amount is less than 0" });
  }
};

module.exports = RequestLoan;
