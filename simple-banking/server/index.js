const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");

const Login = require("./Login");
const SearchAccounts = require("./SearchAccount");
const Register = require("./Register");
const Dashboard = require("./Dashboard");
const AddBalance = require("./AddBalance");
const Payment = require("./Payment");
const RequestLoan = require("./RequestLoan");
const Transfer = require('./Transfer');
const app = express();

mongoose
  .connect(
    "mongodb+srv://bo7mead:1eq9gffeYDlS43Oj@cps731bank.ohr4dru.mongodb.net/?retryWrites=true&w=majority"
  )
  .then(() => console.log("DB CONNECTED!"));

app.use(cors());
app.use(express.json());

// Login Page
app.post("/api/login", (req, res) => {
  Login(req, res);
});

// Search Account
app.post("/api/accounts", (req, res) => {
  SearchAccounts(req, res);
});

// Register Page
app.post("/api/register", (req, res) => {
  Register(req, res);
});

// Dashboard Page
app.get("/dashboard", (req, res) => {
  Dashboard(req, res);
});

// Add Balance
app.post("/api/addbalance", (req, res) => {
  AddBalance(req, res);
});

// Payment
app.post("/api/payment", (req, res) => {
  Payment(req, res);
});

// Request Loan
app.post("/api/request-loan", (req, res) => {
  RequestLoan(req, res);
});

// Transfer Between account

app.post("/api/transfer", (req,res)=>{
  Transfer(req, res);
})
app.listen(8000, () => {
  console.clear();
  console.log("Server Running on port 80");
});
