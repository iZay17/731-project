const User = require("./models/user-model");
const jwt = require("jsonwebtoken");

const Transfer = async (req, res) => {
  const token = jwt.decode(req.headers.token);

  // Try to find The user
  try {
    const user = await User.findOne({ email: token.email });
    const senderAccount = req.body.senderAccount;
    const reciverAccount = req.body.transferTo;
    const amount = req.body.amount;
    let userBalance;
    let reciverBalance;
    let newUserBalance;
    let newReciverBalance;

    // Decide which account to edit
    if (senderAccount === reciverAccount) {
      res.json({ status: "Error", message: "Cannot send to same account!" });
    } else if (senderAccount === "balance") {
      userBalance = user.balance;
      if (userBalance < amount) {
        res.json({
          status: "Error",
          message: "Balance is less than the amount",
        });
      } else if (reciverAccount === "creditCard") {
        reciverBalance = user.creditCard;
        newReciverBalance = reciverBalance + amount;
        newUserBalance = userBalance - amount;

        await User.updateOne(
          { email: token.email },
          { $set: { balance: newUserBalance, creditCard: newReciverBalance } }
        );
        res.json({ status: "ok" });
      } else {
        reciverBalance = user.saving;
        newReciverBalance = reciverBalance + amount;
        newUserBalance = userBalance - amount;
        await User.updateOne(
          { email: token.email },
          { $set: { balance: newUserBalance, saving: newReciverBalance } }
        );
        res.json({ status: "ok" });
      }
    } else if (senderAccount === "creditCard") {
      userBalance = user.creditCard;
      if (userBalance < amount) {
        res.json({
          status: "Error",
          message: "Balance is less than the amount",
        });
      } else if (reciverAccount === "balance") {
        console.log("2");
        reciverBalance = user.balance;
        newReciverBalance = reciverBalance + amount;
        newUserBalance = userBalance - amount;
        await User.updateOne(
          { email: token.email },
          { $set: { creditCard: newUserBalance, balance: newReciverBalance } }
        );
        res.json({ status: "ok" });
      } else {
        reciverBalance = user.saving;
        newReciverBalance = reciverBalance + amount;
        newUserBalance = userBalance - amount;
        await User.updateOne(
          { email: token.email },
          { $set: { creditCard: newUserBalance, saving: newReciverBalance } }
        );
        res.json({ status: "ok" });
      }
    } else if (senderAccount === "saving") {
      userBalance = user.saving;
      if (userBalance < amount) {
        res.json({
          status: "Error",
          message: "Balance is less than the amount",
        });
      } else if (reciverAccount === "balance") {
        reciverBalance = user.balance;
        newReciverBalance = reciverBalance + amount;
        newUserBalance = userBalance - amount;
        await User.updateOne(
          { email: token.email },
          { $set: { saving: newUserBalance, balance: newReciverBalance } }
        );
        res.json({ status: "ok" });
      } else {
        reciverBalance = user.creditCard;
        newReciverBalance = reciverBalance + amount;
        newUserBalance = userBalance - amount;
        await User.updateOne(
          { email: token.email },
          { $set: { saving: newUserBalance, creditCard: newReciverBalance } }
        );
        res.json({ status: "ok" });
      }
    }

    // Update the sender balance
  } catch (error) {
    console.log(error);
  }
};
module.exports = Transfer;
