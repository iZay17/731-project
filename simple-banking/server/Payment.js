const User = require("./models/user-model");
const jwt = require("jsonwebtoken");
const Payment = async (req, res) => {
  const token = jwt.decode(req.headers.token);
  const transferAmount = req.body.amount;
  const transferTo = req.body.transferTo;

  try {
    const senderAccount = req.body.senderAccount;
    const user = await User.findOne({ email: token.email });
    const userRecieve = await User.findOne({ email: transferTo });
    if (senderAccount === "balance") {
      userBalance = user.balance;
    } else if (senderAccount === "saving") {
      userBalance = user.saving;
    }

    const reciverBalance = userRecieve.balance;

    const newReceiverBalance = reciverBalance + transferAmount;
    const newUserBalance = userBalance - transferAmount;

    if (transferAmount > userBalance) {
      return res.json({
        status: "error",
        message: "amount is more than current balance",
      });
    } else if (transferAmount < 0) {
      return res.json({ status: "error", message: "ammount is less than 0" });
    } else {
      try {
        // edit sender balance
        await User.updateOne(
          { email: token.email },
          { $set: { balance: newUserBalance } }
        );

        // edit receiver balance
        await User.updateOne(
          { email: transferTo },
          { $set: { balance: newReceiverBalance } }
        );

        res.json({ status: "ok" });
      } catch (error) {
        res.json({ status: "Error", message: error });
      }
    }
  } catch (error) {
    res.json({ status: "error", message: error });
  }
};
module.exports = Payment;
