const User = require("./models/user-model");
const jwt = require("jsonwebtoken");
const DashBoard = async (req, res) => {
  // Decode the received token
  const token = await jwt.decode(req.headers.token);
  // Token have email and password

  try {
    // Find user by email as of now
    const user = await User.findOne({ email: token.email });
    // return user info as of now
    res.json({ status: "ok", user });
  } catch (error) {
    res.json({ status: "Error", message: error });
  }
};

module.exports = DashBoard;
