const transaction = async (transactionType, transactionAmount, sendAccount, receiveAccount) => {
    const req = await fetch("http://localhost:8000/api/transaction", {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      token: localStorage.getItem("token"),
    },
    body: JSON.stringify({
        transactionType: transactionType,
        transactionAmount: transactionAmount,
        sendAccount: sendAccount,
        receiveAccount: receiveAccount,
    }),
  });

  const res = await req.json();
  if (res.status === "ok") {
    console.log(res.status);
  }
}   

export default transaction;