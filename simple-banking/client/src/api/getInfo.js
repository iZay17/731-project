const getInfo = async () => {
  const req = await fetch("http://localhost:8000/dashboard", {
    method: "GET",
    headers: { token: localStorage.getItem("token") },
  });
  const data = await req.json();
  return data;
};

export default getInfo;
