const transfer = async (accountTo, accountFrom, amount) => {
  const req = await fetch("http://localhost:8000/api/transfer", {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      token: localStorage.getItem("token"),
    },
    body: JSON.stringify({
      amount: amount,
      transferTo: accountTo,
      senderAccount: accountFrom,
    }),
  });

  const res = await req.json();
  console.log(res);
  if (res.status === "ok") {
    alert("Transfer Accepted!");
    window.location.reload();
  } else {
    alert(res.message);
    window.location.reload();
  }
};

export default transfer;
