const payment = async (transferAmount, reciverEmail, senderAccount) => {
  const req = await fetch("http://localhost:8000/api/payment", {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      token: localStorage.getItem("token"),
    },
    body: JSON.stringify({
      amount: transferAmount,
      transferTo: reciverEmail,
      senderAccount: senderAccount,
    }),
  });
  const data = await req.json();
  console.log(data);
  if (data.status === "ok") {
    alert("Transfer Accepted!");
    window.location.reload();
  } else {
    alert(data.message);
  }
};

export default payment;
