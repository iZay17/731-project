import { useNavigate } from "react-router-dom";
import React from "react";
const requestLoan = async (amount, loanType) => {
  const req = await fetch("http://localhost:8000/api/request-loan", {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      token: localStorage.getItem("token"),
    },
    body: JSON.stringify({
      amount: amount,
      loanType: loanType,
    }),
  });

  const res = await req.json();
  if (res.status === "ok") {
    alert("Loan accepted!");
    window.location = "/dashboard";
  }
};

export default requestLoan;
