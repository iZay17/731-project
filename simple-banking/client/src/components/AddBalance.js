const AddBalance = async (balance) => {
  const req = await fetch("http://localhost:8000/api/addbalance", {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      token: localStorage.getItem("token"),
    },
    body: JSON.stringify({ balance: balance }),
  });
  const data = await req.json();

  // Reloading the page to display the updated balance
  if (data.status === "ok") {
    window.location.reload();
  } else {
    alert(data.message);
  }
};

export default AddBalance;
