import React from "react";

const DisplayBalance = (props) => {
  return <span className="balance">{props.balance}$</span>;
};

export default DisplayBalance;
