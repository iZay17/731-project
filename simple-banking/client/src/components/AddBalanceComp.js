import React from "react";
import AddBalance from "./AddBalance";
import { useState } from "react";

const AddBalanceComp = () => {
  const [balance, setBalance] = useState("");
  const submitionHandler = (e) => {
    e.preventDefault();
    // * 1 to convert it to a number
    AddBalance(balance * 1);
  };
  return (
    <div>
      <form onSubmit={submitionHandler}>
        <input
          placeholder="123"
          type="number"
          onChange={(e) => setBalance(e.target.value)}
        ></input>
        <button type="submit">Add Balance</button>
      </form>
    </div>
  );
};

export default AddBalanceComp;
