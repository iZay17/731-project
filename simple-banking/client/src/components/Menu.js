import React from "react";
import PaymentsIcon from "@mui/icons-material/Payments";
import CreditScoreIcon from "@mui/icons-material/CreditScore";
import SendIcon from "@mui/icons-material/Send";
import RequestPageIcon from "@mui/icons-material/RequestPage";

const Menu = () => {

  const logout = () => {
    localStorage.removeItem("token");
    window.location.reload(false);
  }

  return (
    <div className="left-box">
      <span className="temp">LOGO</span>
      <div className="link-box">
        <SendIcon style={{ color: "white" }} />
        <a className="left-menu" href="/transfer">
          Transfer Money
        </a>
      </div>
      <div className="link-box">
        <CreditScoreIcon style={{ color: "white" }} />
        <a className="left-menu" href="#">
          Check Credit Score
        </a>
      </div>
      <div className="link-box">
        <RequestPageIcon style={{ color: "white" }} />
        <a className="left-menu" href="/request-loan">
          Request Loan
        </a>
      </div>
      <div className="link-box">
        <PaymentsIcon style={{ color: "white" }} />
        <a className="left-menu" href="/payment">
          Make a Payment
        </a>
      </div>
      <div className="link-box">
        <PaymentsIcon style={{ color: "white" }} />
        <button className="left-menu" onClick={logout}>
          Logout
        </button>
      </div>
    </div>
  );
};

export default Menu;
