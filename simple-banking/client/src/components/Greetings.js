import React from "react";

const Greetings = (props) => {
  return (
    <span className="greeting">
      Greetings {props.fName} {props.lName}
    </span>
  );
};

export default Greetings;
