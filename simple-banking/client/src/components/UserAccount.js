import React from "react";
import DisplayBalance from "../components/DisplayBalance";
import "../pages/css/dashBoard.css";



const UserAccount = (props) => {

    return(
        <div className="client-container">
        <h2>Client Info</h2>
        <hr className="section-divider"></hr>
        <h4 >Name:{props.fname} {props.lname}</h4>
        <h4 >Email:{props.email}</h4>
        <h4 >Password:{props.pass}</h4>
        <h2>Client Accounts</h2>
        <hr className="section-divider"></hr>
        <div className="account">
          <span className="account-title ">Chequing account</span>
          <div className="account-box">
            <h3 className="account-title -v">Chequing</h3>
            <DisplayBalance balance={props.balance} />
          </div>
        </div>

        <div className="account">
          <span className="account-title">Saving account</span>
          <div className="account-box">
            <h3 className="account-title -v">Saving</h3>
            <DisplayBalance balance={props.saving} />
          </div>
        </div>

        <div className="account">
            <span className="account-title">Credit Card</span>
            <div className="account-box">
                <h3 className="account-title -v">Credit Card</h3>
                <DisplayBalance balance={props.credit} />
            </div>
        </div>
        </div>
    )
}

export default UserAccount;