import React from "react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import jwtDecode from "jwt-decode";
import Greetings from "../components/Greetings";
import DisplayBalance from "../components/DisplayBalance";

import "./css/dashBoard.css";
import Menu from "../components/Menu";

const Dashboard = () => {
  const [fName, setFName] = useState("");
  const [lName, setLName] = useState("");
  const [balance, setBalance] = useState("");
  const [saving, setSaving] = useState("");
  const [credit, setCredit] = useState("");
  const navigate = useNavigate();

  const getAccountInfo = async () => {
    // Send request with the token to backend and find the user
    const req = await fetch("http://localhost:8000/dashboard", {
      method: "GET",
      headers: { token: localStorage.getItem("token") },
    });
    const data = await req.json();

    setFName(data.user.fName);
    setLName(data.user.lName);
    setBalance(data.user.balance);
    setSaving(data.user.saving);
    setCredit(data.user.creditCard);
  };

  useEffect(() => {
    const token = localStorage.getItem("token");

    if (token) {
      const user = jwtDecode(token);
      if (!user) {
        navigate("/login");
        localStorage.removeItem("token");
      } else {
        getAccountInfo();
      }
    } else {
      navigate("/login");
    }
  });

  return (
    <div className="container">
      <Menu />

      <div className="right-box">
        <Greetings fName={fName} lName={lName} />
        <h2 className="account-summary">Account Summary</h2>
        {/* ACC 1 */}
        <div className="account">
          <span className="account-title ">Chequing account</span>
          <div className="account-box">
            <h3 className="account-title -v">Chequing</h3>
            <DisplayBalance balance={balance} />
          </div>
        </div>

        {/* ACC 2 */}
        <div className="account">
          <span className="account-title">Saving account</span>
          <div className="account-box">
            <h3 className="account-title -v">Saving</h3>
            <DisplayBalance balance={saving} />
          </div>
        </div>

        {/* ACC3 */}
        <div className="account">
          <span className="account-title">Credit Card</span>
          <div className="account-box">
            <h3 className="account-title -v">Credit Card</h3>
            <DisplayBalance balance={credit} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
