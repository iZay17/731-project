import React from "react";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import payment from "../api/payment";
import transaction from "../api/transaction";
import jwtDecode from "jwt-decode";
import "../pages/css/transferPage.css";
import Menu from "../components/Menu";
import DisplayBalance from "../components/DisplayBalance";

const Payment = () => {
  const navigate = useNavigate();
  const [balance, setBalance] = useState("");
  const [saving, setSaving] = useState("");
  const [account, setAccount] = useState("balance");
  const [sender, setSender] = useState("");
  const [amount, setAmount] = useState("");
  const [reciver, setReciver] = useState("");

  const formHandler = (e) => {
    e.preventDefault();
    payment(amount * 1, reciver, account);
    transaction("payment", amount * 1, sender, reciver);
  };

  // To check if user is authenticated and get user info
  const getAccountInfo = async () => {
    // Send request with the token to backend and find the user
    const req = await fetch("http://localhost:8000/dashboard", {
      method: "GET",
      headers: { token: localStorage.getItem("token") },
    });
    const data = await req.json();

    setBalance(data.user.balance);
    setSaving(data.user.saving);
    setSender(data.user.email);

    if (data.status != "ok") {
      navigate("/login");
    }
  };

  useEffect(() => {
    const token = localStorage.getItem("token");

    if (token) {
      const user = jwtDecode(token);
      if (!user) {
        navigate("/login");
        localStorage.removeItem("token");
      } else {
        getAccountInfo();
      }
    } else {
      navigate("/login");
    }
  });

  return (
    <div className="container">
      <Menu />
      <div className="right-box">
        <h2 className="account-summary">Account Summary</h2>
        <div className="accounts">
          <div className="account">
            <span className="account-title -v">Chequing account</span>
            <div className="account-box">
              <h3 className="account-title -v">Chequing</h3>
              <DisplayBalance balance={balance} />
            </div>
          </div>
          {/* ACC 2 */}
          <div className="account">
            <span className="account-title -v">Saving account</span>
            <div className="account-box">
              <h3 className="account-title -v">Saving</h3>
              <DisplayBalance balance={saving} />
            </div>
          </div>
        </div>
        <form onSubmit={formHandler}>
          <div className="form-element">
            <label>Enter amount to transfer </label>
            <input
              type="number"
              label="Enter amount to send"
              onChange={(e) => {
                setAmount(e.target.value);
              }}
            ></input>
          </div>
          <div className="form-element">
            <label>Enter email of the recipient </label>
            <input
              placeholder="Enter recipient Email"
              type="text"
              onChange={(e) => {
                setReciver(e.target.value);
              }}
            ></input>
          </div>
          <div className="form-element">
            <label>Select which account </label>
            <select
              // value={account}
              onClick={(e) => {
                setAccount(e.target.value);
              }}
            >
              <option value="balance">Chequing Account</option>
              <option value="saving">Saving Account</option>
            </select>
          </div>
          <div className="btn-box">
            <button className="btn-login">Transfer!</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Payment;
