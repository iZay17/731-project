import React from "react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import jwtDecode from "jwt-decode";
import Greetings from "../components/Greetings";
import SearchIcon from '@mui/icons-material/Search';
import UserAccount from "../components/UserAccount"
import '../pages/css/EmployeeDashboard.css';


import "./css/dashBoard.css";


const SearchBar = ({placeholder, handleSubmit}) => {
  const [userInput, setUserInput] = useState();

  return (
      <div className="search">
          <div className="searchInputs">
              <input type="text" className="bar" placeholder={placeholder} value={userInput} onChange={(e) => setUserInput(e.target.value)}/>
              <button onClick={() => handleSubmit(userInput)}>
                  <SearchIcon />
              </button>
          </div>
      </div>
  )
}

const EmployeeDash = () => {

  const [fName, setFName] = useState("");
  const [lName, setLName] = useState("");
  const [uFName, setuFName] = useState("");
  const [uLName, setuLName] = useState("");
  const [pass, setPassword] = useState("");
  const [balance, setBalance] = useState("");
  const [saving, setSaving] = useState("");
  const [credit, setCredit] = useState("");
  const navigate = useNavigate();
  const [showUser, setShowUser] = useState(false);

    const getAccountInfo = async () => {
        // Send request with the token to backend and find the user
        const req = await fetch("http://localhost:8000/dashboard", {
          method: "GET",
          headers: { token: localStorage.getItem("token") },
        });
        const data = await req.json();
    
        setFName(data.user.fName);
        setLName(data.user.lName);
      };

      useEffect(() => {
        const token = localStorage.getItem("token");
    
        if (token) {
          const user = jwtDecode(token);
          if (!user) {
            navigate("/login");
            localStorage.removeItem("token");
          } else {
            getAccountInfo();
          }
        } else {
          navigate("/login");
        }
      });
      
      const logout = () => {
        localStorage.removeItem("token");
        window.location.reload(false);
      }

      const [email, setEmail] = useState("");
      
      const displayUser = () => {
            setuFName(localStorage.getItem("fName"));
            setuLName(localStorage.getItem("LName"));
            setEmail(localStorage.getItem("email"));
            setPassword(localStorage.getItem("password"));
            setBalance(localStorage.getItem("balance"));
            setCredit(localStorage.getItem("creditCard"));
            setSaving(localStorage.getItem("saving"));
            setShowUser(true);
      }

      async function findUser(email) {

          const respone = await fetch("http://localhost:8000/api/accounts", {
            method: "POST",
            headers: {
              "Content-type": "application/json",
            },
            body: JSON.stringify({
              email,
            }),
          });
          
          const data = await respone.json();
          console.log(data.userData);
          if (data.userData) {
            alert("found user");
            const req = await fetch("http://localhost:8000/dashboard", {
            method: "GET",
            headers: { token: data.userData },
            });
            const info = await req.json();
            localStorage.setItem("fName", info.user.fName);
            localStorage.setItem("lName", info.user.lName);
            localStorage.setItem("email", info.user.email);
            localStorage.setItem("password", info.user.password);
            localStorage.setItem("balance", info.user.balance);
            localStorage.setItem("creditCard", info.user.creditCard);
            localStorage.setItem("saving", info.user.saving);
            displayUser();
          } else {
            alert("failed");
          }
      }

      const handleSubmit = (userInput) => {
        if (userInput) {
          findUser(userInput);
        } else {
          console.log("failed");
        }
      }; 

    return(
        <div className="container">
      <div className="left-box">
        <button className="logout" onClick={logout}>logout</button>
      </div>
        
      <div className="right-box">
        <Greetings fName={fName} lName={lName} />
        <SearchBar placeholder="Search User By Email" handleSubmit={handleSubmit} />
        {showUser && <UserAccount fname={uFName} lname={uLName} email={email} saving={saving} pass={pass} balance={balance} credit={credit}/>}
      </div>
    </div>
    )
}

export default EmployeeDash