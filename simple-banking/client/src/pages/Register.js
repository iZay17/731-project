import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./css/loginPage.css";
import image from "../imgs/intro.png";

const Register = () => {
  const navigate = useNavigate();
  const [fName, setfName] = useState("");
  const [lName, setlName] = useState("");
  const [phoneNumber, setphoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [status, setStatus] = useState("");

  async function registerUser(e) {
    setStatus("Client");
    e.preventDefault();
    const respone = await fetch("http://localhost:8000/api/register", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        fName,
        lName,
        email,
        phoneNumber,
        password,
        status: "Client",
      }),
    });

    const data = await respone.json();
    if (data.status === "ok") {
      navigate("/login");
    } else {
      alert("Error!");
    }
  }

  return (
    <div>
      <div className="grid grid--2cols">
        <div className="login-img">
          <img src={image} />
        </div>
        <div className="form-box">
          <form className="default-form" onSubmit={registerUser}>
            <h2 class="greeting">Welcome Back</h2>
            <div className="form-element">
              <input
                className="text-area"
                placeholder="First Name"
                type="text"
                value={fName}
                onChange={(e) => setfName(e.target.value)}
              ></input>
            </div>
            <div className="form-element">
              <input
                className="text-area"
                placeholder="Last Name"
                type="text"
                value={lName}
                onChange={(e) => setlName(e.target.value)}
              ></input>
            </div>
            <div className="form-element">
              <input
                className="text-area"
                placeholder="Email"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              ></input>
            </div>
            <div className="form-element">
              <input
                className="text-area"
                placeholder="Phone Number"
                type="number"
                value={phoneNumber}
                onChange={(e) => setphoneNumber(e.target.value)}
              ></input>
            </div>
            <div className="form-element">
              <input
                className="text-area"
                placeholder="Password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              ></input>
            </div>
            <div className="btn-box">
              <button className="btn-login" type="submit">
                Register
              </button>
            </div>
            <div className="cta-form">
              <div className="cta-form-element">
                <span className="form-span">Already have an account? </span>
                <a class="link" href="/login">
                  Login
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Register;
