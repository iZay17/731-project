import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Payment from "./Payment";
import Dashboard from "./Dashboard";
import RequestLoan from "./RequestLoan";
import Transfer from "./Transfer";
import EmployeeDash from "./EmployeeDash"
import EmployeeLogin from "./EmployeeLogin"
import UserAccount from "../components/UserAccount"

import Login from "./Login";
import Register from "./Register";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/login" exact element={<Login />} />
          <Route path="/register" exact element={<Register />} />
          <Route path="/dashboard" exact element={<Dashboard />} />
          <Route path="/useraccount" exact element={<UserAccount />} />
          <Route path="/EmployeeLogin" exact element={<EmployeeLogin />} />
          <Route path="/Employeedashboard" exact element={<EmployeeDash />} />
          <Route path="/payment" exact element={<Payment />} />
          <Route path="/transfer" exact element={<Transfer />} />
          <Route path="/request-loan" exact element={<RequestLoan />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
