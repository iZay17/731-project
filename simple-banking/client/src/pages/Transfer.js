import React from "react";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Menu from "../components/Menu";
import DisplayBalance from "../components/DisplayBalance";
import jwtDecode from "jwt-decode";
import transfer from "../api/transfer";

const Transfer = () => {
  const navigate = useNavigate();
  const [balance, setBalance] = useState("");
  const [saving, setSaving] = useState("");
  const [credit, setCredit] = useState("");
  const [amount, setAmount] = useState("");

  const [sendFrom, setSendFrom] = useState("balance");
  const [sendTo, setSendTo] = useState("balance");

  useEffect(() => {
    const token = localStorage.getItem("token");

    if (token) {
      const user = jwtDecode(token);
      if (!user) {
        navigate("/login");
        localStorage.removeItem("token");
      } else {
        getAccountInfo();
      }
    } else {
      navigate("/login");
    }
  });

  const getAccountInfo = async () => {
    // Send request with the token to backend and find the user
    const req = await fetch("http://localhost:8000/dashboard", {
      method: "GET",
      headers: { token: localStorage.getItem("token") },
    });
    const data = await req.json();

    setBalance(data.user.balance);
    setSaving(data.user.saving);
    setCredit(data.user.creditCard);

    if (data.status != "ok") {
      navigate("/login");
    }
  };
  const formHandler = (e) => {
    e.preventDefault();
    transfer(sendTo, sendFrom, amount * 1);
  };

  return (
    <div className="container">
      <Menu />
      <div className="right-box">
        <h2 className="account-summary">Account Summary</h2>
        <div className="accounts">
          <div className="account">
            <span className="account-title ">Chequing account</span>
            <div className="account-box">
              <h3 className="account-title -v">Chequing</h3>
              <DisplayBalance balance={balance} />
            </div>
          </div>
          {/* ACC 2 */}
          <div className="account">
            <span className="account-title ">Saving account</span>
            <div className="account-box">
              <h3 className="account-title -v">Saving</h3>
              <DisplayBalance balance={saving} />
            </div>
          </div>

          {/* ACC 3 */}
          <div className="account">
            <span className="account-title ">Credit Card</span>
            <div className="account-box">
              <h3 className="account-title -v">Credit</h3>
              <DisplayBalance balance={credit} />
            </div>
          </div>
        </div>
        <form onSubmit={formHandler}>
          <div className="form-element">
            <label>Select which account to send to </label>
            <select
              onClick={(e) => {
                setSendTo(e.target.value);
              }}
            >
              <option value="balance">Chequing Account</option>
              <option value="saving">Saving Account</option>
              <option value="creditCard">Credit Card</option>
            </select>
          </div>
          <div className="form-element">
            <label>Enter amount to transfer </label>
            <input
              type="number"
              label="123"
              onChange={(e) => {
                setAmount(e.target.value);
              }}
            ></input>
          </div>
          <div className="form-element">
            <label>Select which account to send from </label>
            <select
              onClick={(e) => {
                setSendFrom(e.target.value);
              }}
            >
              <option value="balance">Chequing Account</option>
              <option value="saving">Saving Account</option>
              <option value="creditCard">Credit Card</option>
            </select>
          </div>
          <div className="btn-box">
            <button className="btn-login">Transfer!</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Transfer;
