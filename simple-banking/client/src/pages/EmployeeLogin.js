import React from "react";
import { useState } from "react";
import "./css/EmployeeLogin.css";
import image from "../imgs/pexels-fauxels-3184339.png";

const EmployeeLogin = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  async function loginUser(e) {
    e.preventDefault();
    const respone = await fetch("http://localhost:8000/api/login", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
      }),
    });

    const data = await respone.json();
    if (data.userData) {
        localStorage.setItem("token", data.userData);
        const req = await fetch("http://localhost:8000/dashboard", {
        method: "GET",
        headers: { token: localStorage.getItem("token") },
        });
  
        const info = await req.json();
        if (info.user.status == "Employee") {
          alert("login sucess");
          window.location.href = "/EmployeeDashboard";
        } else {
          alert("failed");
        }  
      } else {
        alert("failed");
      }
  }

  return (
    <div>
      <div className="grid grid--2cols">
        <div className="login-img">
          <img src={image} />
        </div>
        <div className="form-box">
          <form onSubmit={loginUser} className="default-form">
            <h2 class="greeting">Welcome Back</h2>
            <div className="form-element">
              <input
                className="text-area"
                placeholder="Email"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              ></input>
            </div>

            <div className="form-element">
              <input
                className="text-area"
                placeholder="Password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              ></input>
            </div>
            <div className="btn-box">
              <button className="btn-login" type="submit">
                Login
              </button>
            </div>
            <div className="cta-form">
              <div className="cta-form-element">
                <span className="form-span">Don't have an account? </span>
                <a class="link" href="/register">
                  Sign up
                </a>
              </div>
              <div className="cta-from-element">
                <span className="form-span">Not an employee? </span>
                <a className="link" href="/login">
                  Log in
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default EmployeeLogin;

