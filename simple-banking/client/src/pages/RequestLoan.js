import React from "react";
import { useState } from "react";
import Menu from "../components/Menu";
import requestLoan from "../api/requestLoan";
const RequestLoan = () => {
  const [loanType, setLoan] = useState("fixed");
  const [amount, setAmount] = useState("");

  const formHandler = (e) => {
    e.preventDefault();
    requestLoan(amount * 1, loanType);
  };
  return (
    <div className="container">
      <Menu />
      <div className="right-box">
        <h2 className="account-summary">Request Loan</h2>
        <form onSubmit={formHandler}>
          <div className="form-element">
            <label>Select a loan type </label>
            <select
              onClick={(e) => {
                setLoan(e.target.value);
              }}
            >
              <option value="fixed">Fixed Rate Loan</option>
              <option value="variable">Variable Rate Loan</option>
            </select>
          </div>

          <div className="form-element">
            <label>Enter the requested amount </label>
            <input
              type="number"
              placeholder="123"
              onChange={(e) => {
                setAmount(e.target.value);
              }}
            ></input>
          </div>
          <div className="btn-box">
            <button className="btn-login">Transfer!</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default RequestLoan;
